﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Asynchrone
{
    class Methods
    {
        public static async Task CopyFileAsync(string source, string destination)
        {
            Console.WriteLine("Debut de la copie du fichier source" + source);
            await Task.Run(() => {
                  File.Copy(source, destination);
                }) ;
            Console.WriteLine("Fin de la copie du fichier source" + source);
        }
    }
}
