﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Asynchrone
{
    class Program
    {
        public const string SOURCE_PATH = @"C:\Users\nicod\Downloads";

        public const string DESTINATION_PATH = @"C:\Users\nicod\Documents\Bidon\";

        static async Task Main(string[] args)
        {
            if (!Directory.Exists(DESTINATION_PATH))
                Directory.CreateDirectory(DESTINATION_PATH);

            var files = Directory.GetFiles(SOURCE_PATH);

            List<Task> tasks = new List<Task>();

            foreach (string file in files)
            {
                tasks.Add(Methods.CopyFileAsync(file, DESTINATION_PATH + Path.GetFileName(file)));
            }
            await Task.WhenAll(tasks);
        }
    }
}
